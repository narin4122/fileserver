#!/usr/bin/python
import cgitb
import cgi
cgitb.enable()
print("Content-Type: text/html")
print()
import json,urllib.request
import requests

form = cgi.FieldStorage()
containers = form.getvalue('container')
url = form.getvalue('url')
port = form.getvalue('port')
join = url + ":" + port

restart = "http://" + join + "/containers/" + containers  + "/restart"
requests.post(restart)
