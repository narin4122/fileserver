#!/usr/bin/python
import cgitb
cgitb.enable()
print("Content-Type: text/html")
print()
import json,urllib.request
import cgi


form = cgi.FieldStorage()
url = form.getvalue('url')
port = form.getvalue('port')
joint = url+":"+port

url = "http://" + joint + "/containers/json?all=true"
array = []
#url = "http://128.199.173.5:4243/containers/json?all=true"
try:
        request = urllib.request.urlopen(url,None,2).read().decode("utf-8")
        response = json.loads(request)
        for index in range(0, len(response)):
                name = response[index]['Names']
                name = name[0][1:len(name[0])]
                image = response[index]['Image']
                state = response[index]['State']
                status = response[index]['Status']
                array.append({'Name': name, 'Image': image, 'State': state, 'Status': status})
        data = json.dumps({'Host':array ,'Status':True})
        print (data)
except:
        print(json.dumps({'Status':False}))
