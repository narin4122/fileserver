#!/usr/bin/python
import cgitb
import cgi
cgitb.enable()
print("Content-Type: text/html")
print()
import json,urllib.request
import requests

form = cgi.FieldStorage()
url = form.getvalue('url')
port = form.getvalue('port')
container = form.getvalue('container')
name = form.getvalue('name')

join = url + ":" + port

rename = "http://" + join + "/containers/" + container + "/rename?name=" + name
requests.post(rename)
