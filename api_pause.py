#!/usr/bin/python
import cgitb
import cgi
cgitb.enable()
print("Content-Type: text/html")
print()
import json,urllib.request
import requests

form = cgi.FieldStorage()
containers = form.getvalue('container')
url = form.getvalue('url')
port = form.getvalue('port')
join = url + ":" + port

pause = "http://" + join + "/containers/" + containers  + "/pause"
requests.post(pause)
