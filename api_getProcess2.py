#!/usr/bin/python
import cgitb
cgitb.enable()
print("Content-Type: text/html")
print()
import json,urllib.request
import cgi


form = cgi.FieldStorage()
url = form.getvalue('url')
port = form.getvalue('port')
join = url + ":" + port
container = form.getvalue('container')

url = "http://" + join + "/containers/" + container + "/top?ps_args=aux"
array = []
request = urllib.request.urlopen(url).read().decode("utf-8")
response = json.loads(request)
#print(response)
for index in range(0, len(response['Processes'])):
        name = response['Processes'][index][0]
        process_id = response['Processes'][index][1]
        cpu_use = response['Processes'][index][2]
        mem_use = response['Processes'][index][3]
        array.append({'Name': name,'Process_id':process_id,'Cpu_use':cpu_use,'Mem_use':mem_use })
data = json.dumps({'Host':array})
print (data)
