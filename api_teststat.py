#!/usr/bin/python
import cgitb
import time
import datetime
import pprint
cgitb.enable()
import cgi
# Print necessary headers.
print("Content-Type: text/html")
print()
import json
import requests
import urllib.request
import pymysql

out =[]
db = pymysql.connect(host="localhost",
                     user="root",
                     passwd="Dom546275",
                     db="docker_db")
cur = db.cursor()


form = cgi.FieldStorage()
url = form.getvalue('url')
port = form.getvalue('port')
joint = url + ":" + port


hostURL = 'http://' + joint + '/containers/json'
try:
    request = urllib.request.urlopen(hostURL, None, 1)
    data = json.loads((request.read().decode("utf-8")))
    find = ("SELECT c_name,cpu_per,m_per,net_o FROM usage_db WHERE h_url like '%s' ORDER BY date DESC LIMIT %d"%(joint,len(data)))
    cur.execute(find)
    numrows = int(cur.rowcount)
    for x in range(0, numrows):
       row = cur.fetchone()
       name = row[0]
       cpu = row[1]
       mem = row[2]
       net = row[3]
       out.append({'Name': name, 'Cpu': cpu, 'Mem': mem, 'Net_o': net})
    data2 = json.dumps({'Data': out, 'status': True})
    print(data2)

except:
    request = None
    print("No Data")
